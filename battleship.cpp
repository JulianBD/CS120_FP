/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * Final Project
 * 4/29/16
 * battleship.cpp
 */

#include "battleship.h"

/**
 * BattleshipGame constructor
 */
BattleshipGame::BattleshipGame() {
	setBoard1(Board(10));
	setBoard2(Board(10));
	createP1();
	createP2();
	createShips();
	string str;
	unsigned int a = 0;
	while (a < p1Ships.size()) {
		cout << "PLAYER " << getP1().getName() << " PLACE ";
		cout << p1Ships.at(a).name << ":";
		cin >> str;
		if (makeShips1(str, a)) {
			a++;
		} else {
			cout << "INVALID MOVE" << endl;
		}
	}
	a = 0;
	while (a < p2Ships.size()) {
		cout << "PLAYER " << getP2().getName() << " PLACE ";
		cout << p2Ships.at(a).name << ":";
		cin >> str;
		if (makeShips2(str, a)) {
			a++;
		} else {
			cout << "INVALID MOVE" << endl;
		}
	}
	setCurr(getP1());
	cout << "BEGIN" << endl;
}


/**
 * BattleshipGame constructor for testing
 */
BattleshipGame::BattleshipGame(bool test) {
	if (test) {
		setBoard1(Board(10));
		setBoard2(Board(10));
		createP1();
		createP2();
		createShips();
	}
	setCurr(getP1());
}


/**
 * Handles the input and output for placement of p1Ships on the board
 */
bool BattleshipGame::makeShips1(string str, int a) {
	Coord coord;
    int row;
    int column;
    char orient;

	//quits
	if (str.compare("q") == 0) {
		exit(0);
	}

	//checks if digits
	bool valid = false;
	if (isdigit(str[0]) && isdigit(str[1])) {
		valid = true;
		column = (int)str[0] - 48;
		row = (int)str[1] - 48;
	}

	coord = make_pair(row, column);
	orient = str[2];
	bool good = false;

	//Checks if out of bounds
	if (orient == 'v') {
		if (row + p1Ships.at(a).size <= 10) {
			good = true;
		}
	} else if (orient == 'h') {
		if (column + p1Ships.at(a).size <= 10) {
			good = true;
		}
	}
	if (!valid || !good || !placeShip1(a, coord, orient)) {
		return false;
	} else {
		p1Ships.at(a).first = coord;
		p1Ships.at(a).orientation = orient;
		Ship ship = p1Ships.at(a);
		for (int c = 0; c < ship.size; c++) {
			getBoard1().getGrid()[ship.locations.at(c).first][ship.locations.at(c).second]
				= ship.name.at(0);
		}
		return true;
	}
}


/**
 * Handles the input and output for placement of p2Ships on the board
 */
bool BattleshipGame::makeShips2(string str, int a) {
	Coord coord;
    int row;
    int column;
    char orient;

	//quits
	if (str.compare("q") == 0) {
		exit(0);
	}

	//checks if digits
	bool valid = false;
	if (isdigit(str[0]) && isdigit(str[1])) {
		valid = true;
		column = (int)str[0] - 48;
		row = (int)str[1] - 48;
	}

	coord = make_pair(row, column);
	orient = str[2];
	bool good = false;

	//checks if out of bounds
	if (orient == 'v') {
		if (row + p2Ships.at(a).size <= 10) {
			good = true;
		}
	} else if (orient == 'h') {
		if (column + p2Ships.at(a).size <= 10) {
			good = true;
		}
	}
	if (!valid || !good || !placeShip2(a, coord, orient)) {
		return false;
	} else {
		p2Ships.at(a).first = coord;
   		p2Ships.at(a).orientation = orient;
		Ship ship = p2Ships.at(a);
		for (int c = 0; c < ship.size; c++) {
			getBoard2().getGrid()[ship.locations.at(c).first][ship.locations.at(c).second]
				= ship.name.at(0);
		}
		return true;
	}
}


/**
 * Returns p1Ships
 */
std::vector<Ship> BattleshipGame::getP1Ships() {
	return p1Ships;
}


/**
 * Returns p2Ships
 */
std::vector<Ship> BattleshipGame::getP2Ships() {
	return p2Ships;
}


/**
 * Prints the board showing all ships and player names
 */
void BattleshipGame::printBoard() {
	cout << "PLAYER " << getP1().getName() << " BOARD:" << endl;
	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			cout << getBoard1().getGrid()[a][b] << " ";
		}
		cout << endl;
	}
	cout << "PLAYER " << getP2().getName() << " BOARD:" << endl;
	for (int c = 0; c < 10; c++) {
		for (int d = 0; d < 10; d++) {
			cout << getBoard2().getGrid()[c][d] << " ";
		}
		cout << endl;
	}
}


/**
 * Prints the board from the perspective of player 1, hiding p2's ships
 */
void BattleshipGame::printBoard1() {
	cout << "PLAYER " << getP2().getName() << " BOARD:" << endl;
	for (int c = 0; c < 10; c++) {
		for (int d = 0; d < 10; d++) {
			char ch = getBoard2().getGrid()[c][d];
			if (ch == 'M' || ch == 'H') {
				cout << ch << " ";
			} else {
				cout << "- ";
			}
		}
		cout << endl;
	}
	cout << "YOUR BOARD:" << endl;
	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			cout << getBoard1().getGrid()[a][b] << " ";
		}
		cout << endl;
	}
}


/**
 * Prints the board from the perspective of player 2, hiding p1's ships
 */
void BattleshipGame::printBoard2() {
	cout << getP1().getName() << "'S BOARD:" << endl;
	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			char ch = getBoard1().getGrid()[a][b];
			if (ch == 'M' || ch == 'H') {
				cout << ch << " ";
			} else {
				cout << "- ";
			}
		}
		cout << endl;
	}
	cout << "YOUR BOARD:" << endl;
	for (int c = 0; c < 10; c++) {
		for (int d = 0; d < 10; d++) {
			cout << getBoard2().getGrid()[c][d] << " ";
		}
		cout << endl;
	}
}


/**
 * Checks the coordinate and calls functions to perform the attack
 */
GameResult BattleshipGame::attack_square(Coord coord) {
	int one = coord.first;
	int two = coord.second;
	if (one > 9 || two > 9 || one < 0 || two < 0) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}
	if (!alreadyHit(coord)) {
		checkHit(coord);
	}
	if (checkWin()) {
		if (getCurr().equals(getP1())) {
			cout << "PLAYER " << getP1().getName() << " WON" << endl;
			return RESULT_PLAYER1_WINS;
		} else {
			cout << "PLAYER " << getP2().getName() << " WON" << endl;
			return RESULT_PLAYER2_WINS;
		}
	}

	if (getCurr().equals(getP1())) {
		setCurr(getP2());
	} else {
		setCurr(getP1());
	}
	
	return RESULT_KEEP_PLAYING;
}


/**
 * Always invalid for Battleship, only has meaning for Mobile Battlehip
 */
GameResult BattleshipGame::moveShip(string str) {
	str[0] = ' ';
	cout << "INVALID MOVE" << endl;
	return RESULT_INVALID;
}


/**
 * Simply adds all the ships to p1Ships ad p2Ships
 */
void BattleshipGame::createShips() {
	p1Ships.push_back(Ship("AIRCRAFT CARRIER", 5));
	p2Ships.push_back(Ship("AIRCRAFT CARRIER", 5));
	p1Ships.push_back(Ship("BATTLESHIP", 4));
	p2Ships.push_back(Ship("BATTLESHIP", 4));
	p1Ships.push_back(Ship("CRUISER", 3));
	p2Ships.push_back(Ship("CRUISER", 3));
	p1Ships.push_back(Ship("SUBMARINE", 3));
	p2Ships.push_back(Ship("SUBMARINE", 3));
	p1Ships.push_back(Ship("DESTROYER", 2));
	p2Ships.push_back(Ship("DESTROYER", 2));
}


/**
 * Places a ship for p1 or returns false if it collides with another ship
 */
bool BattleshipGame::placeShip1(int loc, Coord coord, char orient) {
	Coord f = coord;
	std::vector<Coord> temp;
	if (orient == 'h') {
		for (int a = 0; a < p1Ships.at(loc).size; a++) {
			for (unsigned int c = 0; c < p1Ships.size(); c++) {
				if (std::find(p1Ships.at(c).locations.begin(),
						p1Ships.at(c).locations.end(), coord)
						!= p1Ships.at(c).locations.end()) {
					return false;
				}
			}
			temp.push_back(coord);
			coord.second++;
		}
		coord.second--;
	} else if (orient == 'v') {
		for (int b = 0; b < p1Ships.at(loc).size; b++) {
			for (unsigned int c = 0; c < p1Ships.size(); c++) {
				if (std::find(p1Ships.at(c).locations.begin(),
						p1Ships.at(c).locations.end(), coord)
						!= p1Ships.at(c).locations.end()) {
					return false;
				}
			}
			temp.push_back(coord);
			coord.first++;
		}
		coord.first--;
	}
	p1Ships.at(loc).locations = temp;
	p1Ships.at(loc).first = f;
	p1Ships.at(loc).last = coord;
	return true;
}


/**
 * Places a ship for p2 or returns false if it collides with another ship
 */
bool BattleshipGame::placeShip2(int loc, Coord coord, char orient) {
	Coord f = coord;
	std::vector<Coord> temp;
	if (orient == 'h') {
		for (int a = 0; a < p2Ships.at(loc).size; a++) {
			for (unsigned int d = 0; d < p2Ships.size(); d++) {
				if (std::find(p2Ships.at(d).locations.begin(),
						p2Ships.at(d).locations.end(), coord)
						!= p2Ships.at(d).locations.end()) {
					return false;
				}
			}
			temp.push_back(coord);
			coord.second++;
		}
		coord.second--;
	} else if (orient == 'v') {
		for (int b = 0; b < p2Ships.at(loc).size; b++) {
			for (unsigned int d = 0; d < p2Ships.size(); d++) {
				if (std::find(p2Ships.at(d).locations.begin(),
						p2Ships.at(d).locations.end(), coord)
						!= p2Ships.at(d).locations.end()) {
					return false;
				}
			}
			temp.push_back(coord);
			coord.first++;
		}
		coord.first--;
	}
	p2Ships.at(loc).locations = temp;
	p2Ships.at(loc).first = f;
	p2Ships.at(loc).last = coord;
	return true;
}


/**
 * Checks if the spot has already been hit
 */
bool BattleshipGame::alreadyHit(Coord coord) {
	Coord hit;
	if (getCurr().equals(getP2())) {
		for (unsigned int a = 0; a < p1Ships.size(); a++) {
			for (unsigned int b = 0; b < p1Ships.at(a).hits.size(); b++) {
				hit = p1Ships.at(a).hits.at(b);
				if (hit.first == coord.first && hit.second == coord.second) {
					cout << "HIT" << endl;
					return true;
				}
			}
		}
	} else {
		for (unsigned int a = 0; a < p2Ships.size(); a++) {
			for (unsigned int b = 0; b < p2Ships.at(a).hits.size(); b++) {
				hit = p2Ships.at(a).hits.at(b);
				if (hit.first == coord.first && hit.second == coord.second) {
					cout << "HIT" << endl;
					return true;
				}
			}
		}
	}
	return false;
}


/**
 * Checks if the attack is successful and performs operations if it is
 */
bool BattleshipGame::checkHit(Coord coord) {
	if (getCurr().equals(getP2())) {
		for (unsigned int c = 0; c < p1Ships.size(); c++) {
			for (unsigned int e = 0; e < p1Ships.at(c).locations.size(); e++) {
				if (coord.first == p1Ships.at(c).locations.at(e).first
						&& coord.second == p1Ships.at(c).locations.at(e).second) {
					getBoard1().getGrid()[coord.first][coord.second] = 'H';
					p1Ships.at(c).hits.push_back(p1Ships.at(c).locations.at(e));
					p1Ships.at(c).locations.erase(p1Ships.at(c).locations.begin() + e);
					if (p1Ships.at(c).locations.size() == 0) {
						cout << "SUNK " << p1Ships.at(c).name << endl;
						removeShip(c);
						p1Ships.erase(p1Ships.begin() + c);
					} else {
						cout << "HIT" << endl;
					}
					return true;
				}
			}
		}
		getBoard1().getGrid()[coord.first][coord.second] = 'M';
		cout << "MISS" << endl;
		return false;
	} else {
		for (unsigned int d = 0; d < p2Ships.size(); d++) {
			for (unsigned int f = 0; f < p2Ships.at(d).locations.size(); f++) {
				if (coord.first == p2Ships.at(d).locations.at(f).first
						&& coord.second == p2Ships.at(d).locations.at(f).second) {
					getBoard2().getGrid()[coord.first][coord.second] = 'H';
					p2Ships.at(d).hits.push_back(p2Ships.at(d).locations.at(f));
					p2Ships.at(d).locations.erase(p2Ships.at(d).locations.begin() + f);
					if (p2Ships.at(d).locations.size() == 0) {
						cout << "SUNK " << p2Ships.at(d).name << endl;
						removeShip(d);
						p2Ships.erase(p2Ships.begin() + d);
					} else {
						cout << "HIT" << endl;
					}
					return true;
				}
			}
		}
		getBoard2().getGrid()[coord.first][coord.second] = 'M';
		cout << "MISS" << endl;
		return false;
	}
}


/**
 * If a ship is sunk, it changes the grid to show '-' in its place
 */
void BattleshipGame::removeShip(int loc) {
	Coord f;
	char orient;
	if (getCurr().equals(getP2())) {
		f = p1Ships.at(loc).first;
		orient = p1Ships.at(loc).orientation;
		if (orient == 'v') {
			for (int a = 0; a < p1Ships.at(loc).size; a++) {
				getBoard1().getGrid()[f.first][f.second] = '-';
				f.first++;
			}
		} else {
			for (int a = 0; a < p1Ships.at(loc).size; a++) {
				getBoard1().getGrid()[f.first][f.second] = '-';
				f.second++;
			}
		}
	} else {
		f = p2Ships.at(loc).first;
		orient = p2Ships.at(loc).orientation;
		if (orient == 'v') {
			for (int a = 0; a < p2Ships.at(loc).size; a++) {
				getBoard2().getGrid()[f.first][f.second] = '-';
				f.first++;
			}
		} else {
			for (int a = 0; a < p2Ships.at(loc).size; a++) {
				getBoard2().getGrid()[f.first][f.second] = '-';
				f.second++;
			}
		}
	}
}


/**
 * Checks if there is a win
 */
bool BattleshipGame::checkWin() {
	if (p1Ships.size() == 0) {
		return true;
	}
	if (p2Ships.size() == 0) {
		return true;
	}
	return false;
}