#include <iostream>
#include "checkers.h"
#include "battleship.h"
#include "mobilebattleship.h"
#include <cstdio>


/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/29/16
 * games.cpp
 */

using namespace std;

GameResult player_attack(Game& game) {
    Coord coord;
    int row;
    int column;
    string str;
    cout << "PLAYER " << game.getCurr().getName() << ":";
    cin >> str;
    if(str.compare("q") == 0) {
        return RESULT_QUIT;
    }
    if (str.length() == 4) {
        return game.moveShip(str);
    }
    if (str.length() < 2 || str.length() == 3 || str.length() > 3) {
        cout << "INVALID MOVE" << endl;
        return RESULT_INVALID;
    }
    column = (int)str[0] - 48;
    row = (int)str[1] - 48;
    coord = make_pair(row, column);
    return game.attack_square(coord);
}

GameResult checkers_player_attack(CheckersGame& game) {
    Coord coord;
    string str;
    cout << game.getCurr().getName() << ":" << endl;
    cin >> str;
    if(str.compare("q") == 0) {
        return RESULT_QUIT;
    }
    if (str.length() == 4) {
        result = game.moveDriver(str);
        if(result == RESULT_INVALID) {
            cout << "INVALID MOVE\n" << endl;
        }
    }
    return RESULT_INVALID;
}

char getChoice() {
    cout << "CHOOSE A GAME:";
    string str;
    cin >> str;
    return str[0];
}



void Battleship() {
    BattleshipGame bsg;
    GameResult status = RESULT_KEEP_PLAYING;
    do {
        status = player_attack(bsg);
    } while(status != RESULT_PLAYER1_WINS && status != RESULT_PLAYER2_WINS
            && status != RESULT_QUIT);
}

void MobileBattleship() {
    MobileBattleshipGame mbsg;
    GameResult status = RESULT_KEEP_PLAYING;
    do {
        status = player_attack(mbsg);
    } while(status != RESULT_PLAYER1_WINS && status != RESULT_PLAYER2_WINS
            && status != RESULT_QUIT);
}

void Checkers() {
    CheckersGame checkers;
    GameResult status = RESULT_KEEP_PLAYING;
    do {
        status = checkers_player_attack(checkers);
        checkers.printBoard();
    } while(status != RESULT_PLAYER1_WINS && status != RESULT_PLAYER2_WINS
        && status != RESULT_QUIT);

    if(status == RESULT_PLAYER1_WINS) {
        cout << "PLAYER 1 WON\n" << endl;
    } else if(status == RESULT_PLAYER2_WINS) {
        cout << "PLAYER 2 WON\n" << endl;
    }
}

int main() {
    char choice = getChoice();
    while(choice != '1' && choice != '2' && choice != '3' && choice != 'q') {
        cout << "INVALID CHOICE. ";
        choice = getChoice();
    }

    switch (choice) {
        case '1':
            Battleship();
            break;
        case '2':
            MobileBattleship();
            break;
        case '3':
            Checkers();
            break;
        case 'q':
        default:
            return 0;
    }

    return 0;
}
