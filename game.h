#ifndef GAME_H
#define GAME_H
/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * game.h
 */

#include <iostream>
#include <utility>
#include <string>
#include <cassert>
#include <cstdlib>
#include <vector>
#include <array>
#include <algorithm>

using namespace std;


enum GameResult {
	RESULT_KEEP_PLAYING, // turn was valid and game is not over yet
	RESULT_INVALID,      // turn was invalid; e.g. attacked square
	                     // was attacked previously
	RESULT_STALEMATE,    // game over, neither player wins
	RESULT_PLAYER1_WINS, // game over, player 1 wins
	RESULT_PLAYER2_WINS, // game over, player 2 wins
	RESULT_QUIT,
	RESULT_DOUBLE_JUMP
};

typedef std::pair<int, int> Coord;

class Player {

private:
	string name;
	char symbol;

public:
	Player() {
		name = "";
		symbol = '\0';
	}
	Player(string nm, char c) {
		name = nm;
		symbol = c;
	}

	Player(string nm) {
		name = nm;
	}

	string getName() const {
		return name;
	}

	char getSymbol() {
		return symbol;
	}

	bool equals(Player p) {
		if (name.compare(p.getName()) == 0 && symbol == p.getSymbol()) {
			return true;
		}
		return false;
	}
};


class Board {

private:
		int size;
		//Has to be instantiated to a specific size
		//Made it 10x10 but only 3x3 will be filled
		//in a game of ttt.
		char** grid;
		void initGrid();

public:
		Board() {
			size = 1;
			initGrid();
		}

		Board(int sz) {
			size = sz;
			initGrid();
		}

		int getSize() {
			return size;
		}

		char** getGrid() {
			return grid;
		}
};

class Game {

private:
	Board board1, board2;
	Player p1, p2;
	Player current;

public:
	Game();
	virtual ~Game();
	Board getBoard1();
	Board getBoard2();
	void setBoard1(Board b);
	void setBoard2(Board b);
	void createP1();
	void createP2();
	Player getP1();
	Player getP2();
	Player getCurr();
	Player opponent(char player) {
	    if(player == 'O') {
	        return getP2();
	    } else {
	        return getP1();
	    }
	}
	virtual void swapPlayer() {
		if (getCurr().equals(getP1())) {
			setCurr(getP2());
		} else {
			setCurr(getP1());
		}
	}

	void setP1(Player p);
	void setP2(Player p);
	void setCurr(Player p);
	virtual GameResult attack_square(Coord coord);
	virtual GameResult moveShip(string str);
	virtual GameResult moveChecker(string str);
};

#endif
