/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * game.cpp
 */

#include "game.h"

void Game::createP1() {
	string name = "1";
	Player p = Player(name, 'O');
	setP1(p);
}

void Game::createP2() {
	string name = "2";
	Player p = Player(name, 'X');
	setP2(p);
}

Game::Game() {
	setBoard1(board1);
	board2 = Board(1);
	setP1(Player());
	setP2(Player());
}

Game::~Game() {
	for(int i = 0; i < getBoard1().getSize(); i++) {
		delete[] getBoard1().getGrid()[i];
	}
	delete[] getBoard1().getGrid();
		for(int i = 0; i < getBoard2().getSize(); i++) {
		delete[] getBoard2().getGrid()[i];
	}
	delete[] getBoard2().getGrid();

}

void Board::initGrid() {
	grid = new char*[size];
	for (int a = 0; a < size; a++) {
		grid[a] = new char[size];
		for (int b = 0; b < size; b++) {
			grid[a][b] = '-';
		}
	}
}

void Game::setBoard1(Board b){
	board1 = b;
}

void Game::setBoard2(Board b) {
	board2 = b;
}

Board Game::getBoard1() {
	return board1;
}

Board Game::getBoard2() {
	return board2;
}

Player Game::getP1() {
	return p1;
}

Player Game::getP2() {
	return p2;
}

Player Game::getCurr() {
	return current;
}

void Game::setP1(Player p) {
	p1 = p;
}

void Game::setP2(Player p) {
	p2 = p;;
}

void Game::setCurr(Player p) {
	current = p;
}

GameResult Game::attack_square(Coord coord) {
	coord.first++;
	return RESULT_INVALID;
}

GameResult Game::moveShip(string str) {
	str[0] = ' ';
	cout << "INVALID MOVE" << endl;
	return RESULT_INVALID;
}

GameResult Game::moveChecker(string str) {
	str[0] = ' ';
	cout << "INVALID MOVE" << endl;
	return RESULT_INVALID;
}
