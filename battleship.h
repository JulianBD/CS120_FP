#ifndef BATTLESHIP_H
#define BATTLESHIP_H
/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * Final Project
 * 4/29/16
 * battleship.h
 */

#include "game.h"

class Ship {

public:
	string name;
	int size;
	char orientation;
	std::vector<Coord> locations;
	std::vector<Coord> hits;
	Coord first; //leftmost or uppermost
	Coord last; //rightmost or lowermost
	bool afloat;

	/**
	 * Ship constructor
	 */
	Ship(string nm, int sz) {
		name = nm;
		size = sz;
		afloat = true;
	}


	/**
	 * Moves ship locations and hits up
	 */
	void up(int dist) {
		first.first -= dist;
		last.first -= dist;
		for (unsigned int a = 0; a < locations.size(); a++) {
			locations.at(a).first -= dist;
		}
		for (unsigned int b = 0; b < hits.size(); b++) {
			hits.at(b).first -= dist;
		}
	}


	/**
	 * Moves ship locations and hits down
	 */
	void down(int dist) {
		first.first += dist;
		last.first += dist;
		for (unsigned int a = 0; a < locations.size(); a++) {
			locations.at(a).first += dist;
		}
		for (unsigned int b = 0; b < hits.size(); b++) {
			hits.at(b).first += dist;
		}
	}


	/**
	 * Moves ship locations and hits left
	 */
	void left(int dist) {
		first.second -= dist;
		last.second -= dist;
		for (unsigned int a = 0; a < locations.size(); a++) {
			locations.at(a).second -= dist;
		}
		for (unsigned int b = 0; b < hits.size(); b++) {
			hits.at(b).second -= dist;
		}
	}


	/**
	 * Moves ship locations and hits right
	 */
	void right(int dist) {
		first.second += dist;
		last.second += dist;
		for (unsigned int a = 0; a < locations.size(); a++) {
			locations.at(a).second += dist;
		}
		for (unsigned int b = 0; b < hits.size(); b++) {
			hits.at(b).second += dist;
		}
	}

};

class BattleshipGame : public Game {

public:
	std::vector<Ship> p1Ships;
	std::vector<Ship> p2Ships;

	BattleshipGame();
	BattleshipGame(bool test);
	bool makeShips1(std::string str, int a);
	bool makeShips2(std::string str, int a);
	std::vector<Ship> getP1Ships();
	std::vector<Ship> getP2Ships();
	void printBoard();
	void printBoard1();
	void printBoard2();
	virtual GameResult attack_square(Coord coord);
	virtual GameResult moveShip(std::string str);

private:
	void createShips();
	bool placeShip1(int loc, Coord coord, char orient);
	bool placeShip2(int loc, Coord coord, char orient);
	bool alreadyHit(Coord coord);
	bool checkHit(Coord coord);
	void removeShip(int loc);
	bool checkWin();
};



#endif
