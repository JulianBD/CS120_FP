# Isaac Nemzer - inemzer1@jhu.edu
# Julian Dorsey - jdorse28@jhu.edu
# Matt Saltzman - msaltzm5@jhu.edu
# FINAL PROJECT
# 4/15/16
# Makefile

CXX = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CXXFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

.PHONY: all
all: play_checkers play_bs games

games.o: games.cpp game.h
	$(CXX) -c games.cpp $(CXXFLAGS)

play_checkers.o: play_checkers.cpp game.h
	$(CXX) -c play_checkers.cpp $(CXXFLAGS)

play_bs.o: play_bs.cpp game.h
	$(CXX) -c play_bs.cpp $(CXXFLAGS)

play_mbs.o: play_mbs.cpp game.h
	$(CXX) -c play_mbs.cpp $(CXXFLAGS)

games: games.o checkers.o battleship.o mobilebattleship.o game.o
	$(CXX) -o games games.o checkers.o battleship.o mobilebattleship.o game.o $(CXXFLAGS)

play_checkers: play_checkers.o checkers.o game.o
	$(CXX) -o play_checkers play_checkers.o checkers.o game.o $(CXXFLAGS)

checkers.o: checkers.cpp checkers.h game.h
	$(CXX) -c checkers.cpp $(CXXFLAGS)

play_bs: play_bs.o battleship.o mobilebattleship.o game.o
	$(CXX) -o play_bs play_bs.o battleship.o mobilebattleship.o game.o $(CXXFLAGS)

battleship.o: battleship.cpp battleship.h game.h
	$(CXX) -c battleship.cpp $(CXXFLAGS)

mobilebattleship.o: mobilebattleship.cpp mobilebattleship.h battleship.h game.h
	$(CXX) -c mobilebattleship.cpp $(CXXFLAGS)

game.o: game.cpp game.h
	$(CXX) -c game.cpp $(CXXFLAGS)


clean:
	rm -f *.o play_checkers play_bs games
