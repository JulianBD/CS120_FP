#include "checkers.h"
#include <iostream>
#include <stdlib.h>
#include <cassert>


using namespace std;

int main(void) {
    
    CheckersGame checkers;

    GameResult status = RESULT_KEEP_PLAYING;


    //P1 move
    status = checkers.moveDriver("25tl");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 move
    status = checkers.moveDriver("72bl");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 move
    status = checkers.moveDriver("36tl");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 move
    status = checkers.moveDriver("61br");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 move
    status = checkers.moveDriver("47tl");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 move
    status = checkers.moveDriver("63bl");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 jump
    status = checkers.moveDriver("65tl");
    assert(status == RESULT_DOUBLE_JUMP);

    //P1 stay - double jump
    status = checkers.moveDriver("43tr");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 move
    status = checkers.moveDriver("70bl");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 invalid move
    status = checkers.moveDriver("25tl");
    assert(status == RESULT_INVALID);

    //P1 move
    status = checkers.moveDriver("25tr");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 move
    status = checkers.moveDriver("32br");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 move
    status = checkers.moveDriver("76tl");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 double jump
    status = checkers.moveDriver("43bl");
    assert(status == RESULT_DOUBLE_JUMP);

    //P2 --becomes king
    status = checkers.moveDriver("25br");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 invalid on P2 King
    status = checkers.moveDriver("47tr");
    assert(status == RESULT_INVALID);

    //P1 move
    status = checkers.moveDriver("16tr");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 king moves
    status = checkers.moveDriver("47tl");
    assert(status == RESULT_KEEP_PLAYING);

    //P1 move
    status = checkers.moveDriver("07tr");
    assert(status == RESULT_KEEP_PLAYING);

    //P2 King Jumps Double Backward
    status = checkers.moveDriver("36tr");
    assert(status == RESULT_DOUBLE_JUMP);
    //P2 King Move Invalid
    status = checkers.moveDriver("54tr");
    assert(status == RESULT_INVALID);
    //P2 King Takes (End Double)
    status = checkers.moveDriver("54br");
    assert(status == RESULT_KEEP_PLAYING);
    //P1 move
    status = checkers.moveDriver("56tr");
    assert(status == RESULT_KEEP_PLAYING);
    //P2 King Takes Backwards
    status = checkers.moveDriver("76tl");
    assert(status == RESULT_KEEP_PLAYING);
    //P1 move
    status = checkers.moveDriver("14tr");
    assert(status == RESULT_KEEP_PLAYING);
    //P2 attempt of non jump at checker with no jump
    status = checkers.moveDriver("52dl");
    assert(status == RESULT_INVALID);
    //P2 attempt at non jump of checker
    status = checkers.moveDriver("12bl");
    assert(status == RESULT_INVALID);
    //P2 correct jump, no jumps after
    status = checkers.moveDriver("12br");
    assert(status == RESULT_KEEP_PLAYING);
    checkers.printBoard();
    //P1
    status = checkers.moveDriver("25tr");
    assert(status == RESULT_DOUBLE_JUMP);
    //P1
    status = checkers.moveDriver("43tr");
    assert(status == RESULT_KEEP_PLAYING);
    //P2 access non-player checker
    status = checkers.moveDriver("43br");
    assert(status == RESULT_INVALID);













    checkers.printBoard();
    cout << "# P1 Checkers " << checkers.p1Checkers.size() << endl;
    cout << "# P2 Checkers " << checkers.p2Checkers.size() << endl;

    cout << "P1 Checkers:" << endl;
    for(auto it = checkers.p1Checkers.cbegin(); it != checkers.p1Checkers.cend(); it++) {
        cout << it->second->location.first << " " << it->second->location.second << endl;
    }

    cout << "P2 Checkers:" << endl;
    for(auto it = checkers.p2Checkers.cbegin(); it != checkers.p2Checkers.cend(); it++) {
        cout << it->second->location.first << " " << it->second->location.second << endl;
    }
    /*cout << "Checker at (4,3) has jump at: " << checkers.getBoard()[4][3].jumps.at(0).first
         << checkers.getBoard()[4][3].jumps.at(0).second << endl;

         cout << "Checker at (3,6) has jump at: " << checkers.getBoard()[3][6].jumps.at(0).first
              << checkers.getBoard()[3][6].jumps.at(0).second << endl;*/

    return 0;
}
